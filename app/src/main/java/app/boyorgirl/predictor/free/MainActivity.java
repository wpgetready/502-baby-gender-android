package app.boyorgirl.predictor.free;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import app.boyorgirl.predictor.R;

import app.boyorgirl.predictor.free.helper.CuboidButton;
import app.boyorgirl.predictor.free.helper.TinyDB;

import static app.boyorgirl.predictor.R.string.developer_email;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    CuboidButton bloodUpdate, chinessMethod;
    TinyDB tinyDb;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (false) { //only for testing
            Intent intent = new Intent(this, ResultsActivity.class);
            intent.putExtra("isBoy",false);
            intent.putExtra("msg","Blood Update");
            startActivity(intent);
            return;
        }

        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tinyDb = new TinyDB(this);

//        getSupportActionBar().hide();
        setSupportActionBar(toolbar);
        if (toolbar != null){
            toolbar.setTitle("");
        }

        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        chinessMethod = findViewById(R.id.chiness);
        bloodUpdate =   findViewById(R.id.blood);
        chinessMethod.setOnClickListener(this);
        bloodUpdate.setOnClickListener(this);
//
//        if (getSupportActionBar() != null){
//            getSupportActionBar().hide();
//        }

        App app = (App) getApplication();
        LinearLayout adViewLL = findViewById(R.id.adView);
        app.loadAd(adViewLL);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.navchineseMethod) {
            // Handle the camera action
            gotoActivity(ChinessActivity.class);

        } else if (id == R.id.navbloodMethod) {
            gotoActivity(BloodActivity.class);

        } else if (id == R.id.nav_share) {

            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.hi_checkout) + " https://play.google.com/store/apps/details?id=" + getPackageName());
            startActivity(Intent.createChooser(intent, getString(R.string.share)));

        } else if (id == R.id.nav_contact) {
            Intent Email = new Intent(Intent.ACTION_SEND);
            Email.setType("text/email");
            Email.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(developer_email)});
            Email.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.baby_gender_predictor));
            Email.putExtra(Intent.EXTRA_TEXT, getString(R.string.your_suggestion) + "");
            startActivity(Intent.createChooser(Email, getString(R.string.send_feedback)));


        } else if (id == R.id.nav_apps) {
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://developer?id=" + getString(R.string.developer_name))));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=" + getString(R.string.developer_name))));
            }

        } else if (id == R.id.nav_rate) {
            tinyDb.putBoolean("Review",true);
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
        }
//        else if (id == R.id.nav_buy){
//            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=pub:" + getPackageName())));
//        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.chiness:

                gotoActivity(ChinessActivity.class);
                break;
            case R.id.blood:

                gotoActivity(BloodActivity.class);
                break;
        }
    }

    public void gotoActivity(Class<?> cls) {
        Intent intentone = new Intent(MainActivity.this, cls);
        startActivity(intentone);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();


    }


}
