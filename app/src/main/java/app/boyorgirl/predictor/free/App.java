package app.boyorgirl.predictor.free;

import android.app.Application;
import android.graphics.Typeface;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.onesignal.OneSignal;

import app.boyorgirl.predictor.R;


public class App extends Application {
    private static final String COMFORTAA_LIGHT_PATH = "fonts/title.ttf";
    private static final String ROBOTO_LIGHT_PATH = "fonts/roboto_light.ttf";
    private static final String OH_WHALE_FONT ="fonts/ohwhale.ttf";
    public static Typeface comfortaaLight;
    public static Typeface robotoLight;
    public static Typeface ohWhale;
//    private static final String FLURRY_APIKEY = "";
    public static final String LOG_TAG = App.class.getSimpleName();

//    NativeExpressAdView nativeExpressAdView;
    AdView banner;


    @Override
    public void onCreate() {
        super.onCreate();

        //20190517 OneSignal Initialization

        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();



        // Init Flurry
//        FlurryAgent.setLogEnabled(true);
//        FlurryAgent.setLogLevel(Log.INFO);
//
//        FlurryAgent.setVersionName("1.3");
//        FlurryAgent.init(this, FLURRY_APIKEY);
        MobileAds.initialize(this);

//        nativeExpressAdView = new NativeExpressAdView(this);
        banner = new AdView(this);

//        Log.i(LOG_TAG, "Initialized FLurry Agent");
        initTypeface();

//        requestNative();
        requestBanner();
    }

//    public void requestNative(){
//
//        nativeExpressAdView.setAdUnitId(getString(R.string.native_ad));
//
//        nativeExpressAdView.setAdSize(new AdSize(AdSize.FULL_WIDTH, 80));
//
//        nativeExpressAdView.loadAd(new AdRequest.Builder().build());
//    }

    public void requestBanner(){

        banner.setAdUnitId(getString(R.string.banner));

        banner.setAdSize(AdSize.SMART_BANNER);

        banner.loadAd(new AdRequest.Builder().build());
    }

    public void loadAd(LinearLayout layAd) {

        // Locate the Banner Ad in activity xml
        if (banner.getParent() != null) {
            ViewGroup tempVg = (ViewGroup) banner.getParent();
            tempVg.removeView(banner);
        }

        layAd.addView(banner);

    }


    private void initTypeface() {
        comfortaaLight = Typeface.createFromAsset(getAssets(), COMFORTAA_LIGHT_PATH);
        robotoLight = Typeface.createFromAsset(getAssets(), ROBOTO_LIGHT_PATH);
        ohWhale = Typeface.createFromAsset(getAssets(), OH_WHALE_FONT);


    }
}