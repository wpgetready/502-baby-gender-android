package app.boyorgirl.predictor.free;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import app.boyorgirl.predictor.R;
import app.boyorgirl.predictor.free.helper.TinyDB;

/**
 * Created by shivam on 4/2/17.
 *
 */

public class AskRatingDialog extends DialogFragment {

    TinyDB tinyDB;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(
                R.layout.dialog_ask_rating, null);
        builder.setView(view);

        tinyDB = new TinyDB(getActivity());

        TextView mNoBtn = (TextView)view.findViewById(R.id.noBtn);
        TextView mOkBtn = (TextView)view.findViewById(R.id.okBtn);

        mNoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getDialog().dismiss();
                Intent intentone = new Intent(getActivity(), MainActivity.class);
                startActivity(intentone);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);

            }
        });

        mOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tinyDB.putBoolean("Review",true);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getActivity().getPackageName())));
                getDialog().dismiss();
//                Intent intentone = new Intent(getActivity(), MainActivity.class);
//                startActivity(intentone);
//                getActivity().finish();
//                getActivity().overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);

            }
        });

        return builder.create();
    }
}
