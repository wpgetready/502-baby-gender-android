package app.boyorgirl.predictor.free;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import app.boyorgirl.predictor.R;
import app.boyorgirl.predictor.free.helper.TinyDB;

public class BloodActivity extends AppCompatActivity implements View.OnClickListener {
    EditText momAgeConcp, dadAgeConcp;
    private double dadAgeDevided = 0.0d;
    private double momAgeDevided = 0.0d;
    Button calculation;
    LinearLayout linearLayout;

    TinyDB tinyDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blood);
        momAgeConcp =  findViewById(R.id.mothrBlood);
        dadAgeConcp =  findViewById(R.id.dadBlood);
        calculation =  findViewById(R.id.bloodCalculate);
        linearLayout = findViewById(R.id.bloodLinear);

        calculation.setOnClickListener(this);
        tinyDB = new TinyDB(this);

        if (getSupportActionBar() != null){
            getSupportActionBar().hide();
        }
        linearLayout.setVisibility(View.VISIBLE);
        App app = (App) getApplication();
        LinearLayout adViewLL = findViewById(R.id.adView);
        app.loadAd(adViewLL);
    }

    public void calculate() {
        int momAgeAtConception = Integer.parseInt(momAgeConcp.getText().toString());
        int dadAgeAtConception = Integer.parseInt(dadAgeConcp.getText().toString());
        dadAgeDevided = (((double) dadAgeAtConception) / 4.0d) - ((double) (dadAgeAtConception / 4));
        momAgeDevided = (((double) momAgeAtConception) / 3.0d) - ((double) (momAgeAtConception / 3));

        Intent intent = new Intent(this, ResultsActivity.class);
        intent.putExtra("msg",getString(R.string.blood));
        intent.putExtra("isBoy",(momAgeDevided>=dadAgeDevided));
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bloodCalculate:
                if (!momAgeConcp.getText().toString().equals("") && !dadAgeConcp.getText().toString().equals("")){
                    View view = getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                    int momAgeAtConception = Integer.parseInt(momAgeConcp.getText().toString());
                    int dadAgeAtConception = Integer.parseInt(dadAgeConcp.getText().toString());
                    if ((momAgeAtConception >= 18 && momAgeAtConception <= 50) && dadAgeAtConception >= 18 && dadAgeAtConception <= 60) {
                        calculate();
                    }else {
                        Toast.makeText(this, R.string.please_enter_valid_input, Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(this, R.string.please_enter_valid_input, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onResume() {super.onResume(); }

    @Override
    public void onBackPressed() {
        if (!tinyDB.getBoolean("Review")){
            AskRatingDialog dialogFragment = new AskRatingDialog();
            dialogFragment.show(getFragmentManager(),getResources().getString(R.string.app_name));
        }else {
            Intent intentone = new Intent(BloodActivity.this, MainActivity.class);
            startActivity(intentone);
            finish();
            overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        }
    }
}