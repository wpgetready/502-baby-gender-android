package app.boyorgirl.predictor.free.helper;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import app.boyorgirl.predictor.free.App;

public class BabyTextView extends TextView {
    public BabyTextView(Context context) {
        this(context, null);
    }

    public BabyTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BabyTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        //setTypeface(App.comfortaaLight);
        setTypeface(App.ohWhale);

    }

}