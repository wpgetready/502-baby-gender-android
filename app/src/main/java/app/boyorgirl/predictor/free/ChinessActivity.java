package app.boyorgirl.predictor.free;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import app.boyorgirl.predictor.R;

import app.boyorgirl.predictor.free.helper.TinyDB;

public class ChinessActivity extends AppCompatActivity {
    Spinner monthSpinner;
    EditText mothrAge;
    String month;
    Button calc;
    ImageView mImage;
    int motherAge = 0;
    ImageView infoBtn;

    private final String JAN = "January";
    private final String FEB = "February";
    private final String MAR = "March";
    private final String APR = "April";
    private final String MAY = "May";
    private final String JUN = "June";
    private final String JUL = "July";
    private final String AUG = "August";
    private final String SEP = "September";
    private final String OCT = "October";
    private final String NOV = "November";
    private final String DEC = "December";

    private final String GIRL = "Girl";
    private final String BOY = "Boy";

    TinyDB tinyDB;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle bundle) {

        super.onCreate(bundle);
        setContentView(R.layout.activity_chinese);

        mothrAge =      findViewById(R.id.mothrAge);
        monthSpinner =  findViewById(R.id.spinner);
        calc =          findViewById(R.id.calculate);
        mImage =        findViewById(R.id.childImage);
        final LinearLayout linearLayout =  findViewById(R.id.container);

        infoBtn = findViewById(R.id.imgBtn);
        tinyDB = new TinyDB(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        calc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mothrAge.getText().toString().equals("")) {
                    View view = getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                    motherAge = Integer.parseInt(mothrAge.getText().toString());
                    if (motherAge != 0 && motherAge < 18 || motherAge > 45) {
                        Toast.makeText(ChinessActivity.this, R.string.please_correct_date, Toast.LENGTH_SHORT).show();
                    } else {
                        calculate();
                    }
                } else {
                    mothrAge.setError(getString(R.string.cant_be_empty));
                }
            }
        });

        linearLayout.setVisibility(View.VISIBLE);
        month = String.valueOf(monthSpinner.getSelectedItem());
        monthSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                month = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });

        infoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InfoDialog infoDialog = new InfoDialog();
                infoDialog.show(getFragmentManager(), "");
            }
        });

        //Loading Native
        App app = (App) getApplication();
        LinearLayout adViewLL = (LinearLayout)findViewById(R.id.adView);
        app.loadAd(adViewLL);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void calculate() {
        month = monthSpinner.getSelectedItem().toString();
        Log.e("month", month + " : " + motherAge);

        switch (motherAge) {
            case 18:
                if (month.equals(JAN) || month.equals(MAR)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 19:
                if (month.equals(FEB) || month.equals(APR) || month.equals(MAY) || month.equals(NOV) || month.equals(DEC)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 20:
                if (month.equals(JAN) || month.equals(MAR) || month.equals(OCT)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 21:
                if (month.equals(FEB) || month.equals(MAR) || month.equals(APR) || month.equals(MAY) || month.equals(JUN) ||
                        month.equals(JUL) || month.equals(AUG) || month.equals(SEP) || month.equals(OCT) || month.equals(NOV) || month.equals(DEC)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 22:
                if (month.equals(JAN) || month.equals(APR) || month.equals(JUN) || month.equals(JUL) || month.equals(SEP) ||
                        month.equals(OCT) || month.equals(NOV) || month.equals(DEC)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
            case 23:
                if (month.equals(MAR) || month.equals(JUN) || month.equals(AUG) || month.equals(DEC)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 24:
                if (month.equals(FEB) || month.equals(MAY) || month.equals(AUG) || month.equals(SEP) || month.equals(OCT) || month.equals(NOV) || month.equals(DEC)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 25:
                if (month.equals(JAN) || month.equals(APR) || month.equals(MAY) || month.equals(JUL)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 26:
                if (month.equals(FEB) || month.equals(APR) || month.equals(MAY) || month.equals(JUL)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 27:
                if (month.equals(JAN) || month.equals(MAR) || month.equals(MAY) || month.equals(JUN) || month.equals(NOV)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 28:
                if (month.equals(FEB) || month.equals(APR) || month.equals(MAY) || month.equals(JUN) || month.equals(NOV) || month.equals(DEC)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 29:
                if (month.equals(JAN) || month.equals(MAR) || month.equals(APR) || month.equals(OCT) || month.equals(NOV) || month.equals(DEC)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 30:
                if (month.equals(FEB) || month.equals(MAR) || month.equals(APR) || month.equals(MAY) || month.equals(JUN)
                        || month.equals(JUL) || month.equals(AUG) || month.equals(SEP) || month.equals(OCT)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 31:
                if (month.equals(FEB) || month.equals(APR) || month.equals(MAY) || month.equals(MAY) || month.equals(JUN) || month.equals(JUL)
                        || month.equals(AUG) || month.equals(SEP) || month.equals(OCT) || month.equals(NOV)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 32:
                if (month.equals(FEB) || month.equals(APR) || month.equals(MAY) || month.equals(JUN) || month.equals(JUL) || month.equals(AUG)
                        || month.equals(SEP) || month.equals(OCT) || month.equals(NOV)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 33:
                if (month.equals(JAN) || month.equals(MAR) || month.equals(MAY) || month.equals(JUN) || month.equals(JUL)
                        || month.equals(SEP) || month.equals(OCT) || month.equals(NOV)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 34:
                if (month.equals(JAN) || month.equals(APR) || month.equals(MAY) || month.equals(JUN) || month.equals(JUL) || month.equals(SEP)
                        || month.equals(OCT)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 35:
                if (month.equals(MAR) || month.equals(MAY) || month.equals(JUN) || month.equals(JUL) || month.equals(SEP) || month.equals(OCT)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 36:
                if (month.equals(JAN) || month.equals(APR) || month.equals(JUN) || month.equals(JUL) || month.equals(AUG)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 37:
                if (month.equals(FEB) || month.equals(MAY) || month.equals(JUN) || month.equals(JUL) || month.equals(SEP) || month.equals(NOV)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 38:
                if (month.equals(JAN) || month.equals(MAR) || month.equals(JUN) || month.equals(AUG) || month.equals(OCT) || month.equals(DEC)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 39:
                if (month.equals(FEB) || month.equals(JUN) || month.equals(JUL) || month.equals(SEP) || month.equals(NOV) || month.equals(DEC)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 40:
                if (month.equals(JAN) || month.equals(MAR) || month.equals(MAY) || month.equals(AUG) || month.equals(OCT) || month.equals(DEC)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 41:
                if (month.equals(JAN) || month.equals(APR) || month.equals(JUN) || month.equals(SEP) || month.equals(NOV)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 42:
                if (month.equals(JAN) || month.equals(MAR) || month.equals(MAY) || month.equals(JUL) || month.equals(OCT) || month.equals(DEC)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 43:
                if (month.equals(FEB) || month.equals(APR) || month.equals(JUN) || month.equals(AUG)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 44:
                if (month.equals(MAR) || month.equals(JUL) || month.equals(SEP) || month.equals(NOV) || month.equals(DEC)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;
            case 45:
                if (month.equals(JAN) || month.equals(APR) || month.equals(MAY) || month.equals(JUN) || month.equals(AUG) || month.equals(OCT)) {
                    mCalculate(GIRL);
                } else {
                    mCalculate(BOY);
                }
                break;

            default:
//                Toast.makeText(this, "TODO", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void mCalculate(String result) {
        Intent intent = new Intent(this, ResultsActivity.class);
        intent.putExtra("msg",getString(R.string.chinese));
        intent.putExtra("isBoy",(result.equals(BOY)));
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (!tinyDB.getBoolean("Review")) {
            AskRatingDialog dialogFragment = new AskRatingDialog();
            dialogFragment.show(getFragmentManager(),
                    getResources().getString(R.string.app_name));
        } else {
            Intent intentone = new Intent(ChinessActivity.this, MainActivity.class);
            startActivity(intentone);
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
    }
}