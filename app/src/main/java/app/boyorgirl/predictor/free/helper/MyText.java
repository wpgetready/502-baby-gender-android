package app.boyorgirl.predictor.free.helper;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import app.boyorgirl.predictor.free.App;

public class MyText extends TextView {


    public MyText(Context context) {
        this(context, null);
    }

    public MyText(Context context,  AttributeSet attrs) {
        this(context, attrs,0);
    }

    public MyText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(App.robotoLight);
    }
}