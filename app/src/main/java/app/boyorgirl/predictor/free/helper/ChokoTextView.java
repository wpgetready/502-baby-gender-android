package app.boyorgirl.predictor.free.helper;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import app.boyorgirl.predictor.free.App;

public class ChokoTextView extends TextView {
    public ChokoTextView(Context context) {
        this(context, null);
    }

    public ChokoTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChokoTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
       setTypeface(App.comfortaaLight);
       //setTypeface(App.ohWhale);

    }

}
