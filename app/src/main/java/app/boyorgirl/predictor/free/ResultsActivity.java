package app.boyorgirl.predictor.free;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.todddavies.components.progressbar.ProgressWheel;

import app.boyorgirl.predictor.R;
import app.boyorgirl.predictor.free.helper.BabyTextView;
import app.boyorgirl.predictor.free.helper.ChokoTextView;

import static android.view.View.GONE;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class ResultsActivity extends AppCompatActivity {

    /*Customized vars and configuration*/
    public static final int WAIT_BEFORE_AD = 3000;
    public static final int WAIT_BEFORE_CALC = 3000;
    FrameLayout progressFrame; //FZSM
    FrameLayout results;
    ProgressWheel pw;
    TextView fullscreenContent;
    Boolean isBoy = true; //it is also used by the share button
    /*#####################################################*/

    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private View mControlsView;
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            mControlsView.setVisibility(View.VISIBLE);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_results);

        mVisible = true;
        mControlsView = findViewById(R.id.fullscreen_content_controls);
        mContentView = findViewById(R.id.fullscreen_content);
        fullscreenContent =findViewById(R.id.fullscreen_content);


        // Set up the user interaction to manually show or hide the system UI.
        mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.

        //20191017:Este boton se encuentra oculto en un frame y lo he dejado sin tocar a efectos de estudiar la documentación de este ejemplo
        findViewById(R.id.dummy_button).setOnTouchListener(mDelayHideTouchListener);

        progressFrame = findViewById(R.id.progressFrame);
        progressFrame.setVisibility(GONE);
        results =findViewById(R.id.resultLayout);
        results.setVisibility(GONE);
        initializeAds();
        waitBeforeDisplayAd(WAIT_BEFORE_AD); //Esperar antes de desplegar el aviso, luego proceder con el intersticial.
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }


    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mControlsView.setVisibility(GONE);
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in delay milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    //El codigo de esta app empieza aqui, el resto fue autogenerado.###################################################################
    private InterstitialAd interstitialAd;

    private void initializeAds() {
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.interstitial));
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {}

            @Override
            public void onAdClosed() {
                spinWheel();
            }
        });

        AdRequest interstitialRequest = new AdRequest.Builder().build();
        interstitialAd.loadAd(interstitialRequest);

        //Por el momento, banners no,pero versiones posteriores harán uso y abuso de las mismas. (2 networks)
        /*
        App app = (App) getApplication();
        LinearLayout adViewLL = (LinearLayout)findViewById(R.id.adView);
        app.loadAd(adViewLL);
        */
    }

    private static final int STOP_PROCESS =0;
    private Handler waitTime = new Handler() {
        /* (non-Javadoc)
         * @see android.os.Handler#handleMessage(android.os.Message)
         */
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case STOP_PROCESS:
                    if (interstitialAd.isLoaded()) {
                    interstitialAd.show(); //display results will be handled after interstitial is closed (see event)
                } else {
                        //Ad wasn't ready or there is not network, go on.
                        spinWheel();
                    }
                    break;
            }
            super.handleMessage(msg);
        }
    };

    /**
     * Esperar x milisegundos antes de lanzar un handler con un delay determinado en milisegundos.
     * @param miliseconds
     */

    private void waitBeforeDisplayAd(int miliseconds) {
        Message msg = new Message();
        msg.what = STOP_PROCESS;
        waitTime.sendMessageDelayed(msg, miliseconds);
    }

    private Handler progressWheelHandler = new Handler() {
        /* (non-Javadoc)
         * @see android.os.Handler#handleMessage(android.os.Message)
         */
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case STOP_PROCESS:
                    pw.stopSpinning();
                    pw.setVisibility(GONE);
                    displayResults();
                    break;
            }
            super.handleMessage(msg);
        }
    };

    private void spinWheel() {
        fullscreenContent.setVisibility(GONE);
        progressFrame.setVisibility(View.VISIBLE);
        pw= findViewById(R.id.progressWheel);
        pw.startSpinning();
        Message m = new Message();
        m.what=STOP_PROCESS;
        int wait = WAIT_BEFORE_CALC + (int)(Math.random()  * 1500);
        progressWheelHandler.sendMessageDelayed(m, wait);
    }

    private void displayResults() {
//TODO:get the data from the intent
//display results
        Intent intent = getIntent();
        isBoy = intent.getBooleanExtra("isBoy",true);
        String msg = intent.getStringExtra("msg");
        setRibbon(isBoy,msg);
        results.setVisibility((View.VISIBLE));
    }

    private void setRibbon(boolean isBoy,String msg) {
        ImageView resultImage =findViewById(R.id.childImage);
        BabyTextView babyTextView = findViewById(R.id.txtMsg);
        ChokoTextView titleTextView = findViewById(R.id.chokoTextView);

        titleTextView.setText(msg);


        if (!isBoy) {

            resultImage.setImageResource(R.drawable.girl_result);
            babyTextView.setText(getString(R.string.it_s_a_girl));

            //Fzsm: depending android version execute one or another
            //to be honest i don't know if it this going to work, but Android Studio FORCED ME to update from 16 to 21
            //I tried to go back and I couldn't (or at least, AS accepted the code that it didnt')
            //So this is my if anything goes wrong.
            if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
                babyTextView.setBackgroundDrawable(getResources().getDrawable(R.drawable.ribbonpink));
            } else {
                babyTextView.setBackgroundResource(R.drawable.ribbonpink);
            }

        } else {

            resultImage.setImageResource(R.drawable.boy_result);
            babyTextView.setText(getString(R.string.it_s_a_boy));

            if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
                babyTextView.setBackgroundDrawable(getResources().getDrawable(R.drawable.ribbonblue));
            }
            else
            {
                babyTextView.setBackgroundResource(R.drawable.ribbonblue);
            }

        }
    }

    @SuppressLint("StringFormatInvalid")
    public void shareResult(View view) {

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String Method = getString(R.string.blood);
        String methodTranslation = (isBoy?getString(R.string.boy):getString(R.string.girl));
        intent.putExtra(Intent.EXTRA_TEXT,getString(R.string.according_to,Method,methodTranslation) + "  " + getString(R.string.download_the_app_here) +
                "https://play.google.com/store/apps/details?id=" + getPackageName());

        startActivity(Intent.createChooser(intent, getResources().getString(R.string.share)));
    }
}
